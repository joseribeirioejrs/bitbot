const mercadoBTC = {
    endpoint: 'https://www.mercadobitcoin.net/api/',
    coin: {
        bitcoin: 'BTC',
        litecoin: 'LTC', 
        bitcoinCash: 'BCH', 
        ripple: 'XRP',
        ethereum: 'ETH' 
    },
    method: {
        ticker: 'ticker', // resumo de operações executadas
        orderbook: 'orderbook', // livro de negociações, ordens abertas de compra e venda
        trades: 'trades' // histórico de operações executadas
    }
};

module.exports = {
    endpoints: {
        mercadoBTC: {
            bitcoinPrice: `${mercadoBTC.endpoint}${mercadoBTC.coin.bitcoin}/${mercadoBTC.method.orderbook}`,
            litecoinPrice: `${mercadoBTC.endpoint}${mercadoBTC.coin.litecoin}/${mercadoBTC.method.orderbook}`,
            bitcoinCashPrice: `${mercadoBTC.endpoint}${mercadoBTC.coin.bitcoinCash}/${mercadoBTC.method.orderbook}`,
            ripplePrice: `${mercadoBTC.endpoint}${mercadoBTC.coin.ripple}/${mercadoBTC.method.orderbook}`,
            ethereumPrice: `${mercadoBTC.endpoint}${mercadoBTC.coin.ethereum}/${mercadoBTC.method.orderbook}`
        }
    }
}
