const environment = require("./environment");
const axios = require("axios");

const tax = {
	active: 0.3,
	inactive: 0.7
}

const typeOrder = {
	buy: 'bids',
	sell: 'asks'
}

const isValidType = (type) => {
	if (type !== 'asks' && type !== 'bids') {
		return false;
	}
	return true;
}

/**
 * O mercado bitcoin retorna as ordens da seguinte forma 
 * [10410.00006000, 2.09190016]
 * Sendo o 10410.00006000 o valor a ser negociado e
 * 2.09190016 a quantidade de Bitcoin a esse preço
 */

const makeOrderObject = (rawOrders) => {
	const order = {};
	rawOrders && rawOrders.forEach((rawOrder, index) => {
		if (index === 0) {
			order.price = rawOrder;
		} else if (index === 1) {
			order.amount = rawOrder;
		}
	});
	return order;
}

const makeOrders = (rawOrders) => {
	const orders = [];
	rawOrders && rawOrders.forEach(rawOrder => {
		orders.push(makeOrderObject(rawOrder));
	})
	return orders;
}

const getPrices = () => {
	return axios.get(environment.endpoints.mercadoBTC.bitcoinPrice);
}

module.exports = {
	makeOrderObject,
	makeOrders,
	typeOrder,
	isValidType,
	getPrices,
	tax
}