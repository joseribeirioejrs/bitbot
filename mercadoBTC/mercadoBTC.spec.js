const MercadoBTC = require('./index');
const assert = require('assert');

describe('MercadoBTC', () => {
	it('makeOrderObject', () => {
		const rawOrder = [10410.00006000, 2.09190016];

		const order = MercadoBTC.makeOrderObject(rawOrder);

		assert.equal(order.price, rawOrder[0]);
		assert.equal(order.amount, rawOrder[1]);
	});

	it('makeOrders', () => {
		const rawOrders = [
			[10410.00006000, 2.09190016],
			[10420.00000000, 0.00997000],
			[10488.99999000, 0.46634897]
		];

		const orders = MercadoBTC.makeOrders(rawOrders);

		orders.forEach((order, index) => {
			if (!!order) {
				assert.equal(order.price, rawOrders[index][0]);
				assert.equal(order.amount, rawOrders[index][1]);
			}
		})
	});

	it('isValidOrder buy', () => {
		assert.equal(true, MercadoBTC.isValidType(MercadoBTC.typeOrder.buy));
	});
	
	it('isValidOrder sell', () => {
		assert.equal(true, MercadoBTC.isValidType(MercadoBTC.typeOrder.sell));
	});

	it('isValidOrder orther', () => {
		assert.equal(false, MercadoBTC.isValidType('invalid type'));
	});
});