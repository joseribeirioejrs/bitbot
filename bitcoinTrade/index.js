const environment = require("./environment");
const axios = require("axios");

const tax = {
	active: 0.25,
	inactive: 0.5
}

const makeCommonObject = (rawOrders) => {
	return rawOrders && rawOrders.map(rawOrder => {
		return {
			price: rawOrder.unit_price,
			amount: rawOrder.amount
		}
	});
}

const getPrices = () => {
	return axios.get(environment.endpoints.bitcoinTrade.bitcoinPrice);
}

module.exports = {
	getPrices,
	makeCommonObject,
	tax
}