const BitcoinTrade = require('./index');
const assert = require('assert');

describe('BitcoinTrade', () => {
	it('makeCommonObject', () => {
		const rawOrders = [
			{
				unit_price: 14650.25,
				code: "B1qRRuFBN",
				amount: 0.46097295
			},
			{
				unit_price: 14650.25,
				code: "B1dcgFKrE",
				amount: 1
			}
		];

		const orders = BitcoinTrade.makeCommonObject(rawOrders);

		rawOrders.forEach((rawOrder, index) => {
			assert.equal(rawOrder.unit_price, orders[index].price);
			assert.equal(rawOrder.amount, orders[index].amount);
		});
	});
});