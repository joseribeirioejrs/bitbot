const bitcoinTrade = {
    endpoint: 'https://api.bitcointrade.com.br/v3/public/',
    coin: {
        bitcoin: 'BRLBTC',
        litecoin: 'BRLLTC', 
        bitcoinCash: 'BRLBCH', 
        ripple: 'BRLXRP',
        ethereum: 'BRLETH' 
    },
    method: {
        ticker: 'ticker', // resumo de operações executadas
        orderbook: 'orders', // livro de negociações, ordens abertas de compra e venda
        trades: 'trades' // histórico de operações executadas
    }
};

module.exports = {
    endpoints: {
        bitcoinTrade: {
            bitcoinPrice: `${bitcoinTrade.endpoint}${bitcoinTrade.coin.bitcoin}/${bitcoinTrade.method.orderbook}`,
            litecoinPrice: `${bitcoinTrade.endpoint}${bitcoinTrade.coin.litecoin}/${bitcoinTrade.method.orderbook}`,
            bitcoinCashPrice: `${bitcoinTrade.endpoint}${bitcoinTrade.coin.bitcoinCash}/${bitcoinTrade.method.orderbook}`,
            ripplePrice: `${bitcoinTrade.endpoint}${bitcoinTrade.coin.ripple}/${bitcoinTrade.method.orderbook}`,
            ethereumPrice: `${bitcoinTrade.endpoint}${bitcoinTrade.coin.ethereum}/${bitcoinTrade.method.orderbook}`
        }
    }
}
