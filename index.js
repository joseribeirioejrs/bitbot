const MercadoBTC = require('./mercadoBTC/index');
const BitcoinTrade = require('./bitcoinTrade/index');
const ProcessData = require('./processData/index');

const BitcoinTradePromise = BitcoinTrade.getPrices();
const MercadoBTCPromise = MercadoBTC.getPrices();

setInterval(() => {
	Promise
		.all([BitcoinTradePromise, MercadoBTCPromise])
		.then(orderbooks => {
			ProcessData.processBTCPriceBitcoinTrade(orderbooks[0]);
			ProcessData.processPriceBTCMercadoBTC(orderbooks[1]);
			ProcessData.gapsMercadoBTCAndBitcoinTrade();
		}).catch(error => {
			console.log(error);
		});
}, 2000);

