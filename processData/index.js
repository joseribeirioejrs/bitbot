const MercadoBTC = require('../mercadoBTC/index');
const BitcoinTrade = require('../bitcoinTrade/index');

const DIFFERENCE_TO_BUY = 0.02;

const mercadoBTCPrices = {
	buy: [],
	sell: []
}

const bitcoinTradePrices = {
	buy: [],
	sell: []
}

const processBTCPriceBitcoinTrade = (orderbook) => {
	console.log('Dados coletados do BitcoinTrade');
	const data = orderbook && orderbook.data;
	bitcoinTradePrices.buy = data && data.data && BitcoinTrade.makeCommonObject(data.data.bids);
	bitcoinTradePrices.sell = data && data.data && BitcoinTrade.makeCommonObject(data.data.asks);
	// console.log(bitcoinTradePrices); 
}

const processPriceBTCMercadoBTC = (orderbook) => {
	console.log('Dados coletados do Mercado Bitcoin')
	mercadoBTCPrices.buy = orderbook && orderbook.data && MercadoBTC.makeOrders(orderbook.data.bids); 
	mercadoBTCPrices.sell = orderbook && orderbook.data && MercadoBTC.makeOrders(orderbook.data.asks);
	// console.log(mercadoBTCPrices); 
}

const gapsMercadoBTCAndBitcoinTrade = () => {
	const buyMercadoBTCSellBitcoinTrade  = mercadoBTCPrices.buy[0].price / bitcoinTradePrices.sell[0].price;
	const buyBitconTradeSellMercadoBTC  = bitcoinTradePrices.buy[0].price / mercadoBTCPrices.sell[0].price;

	console.log('GAP comprar no Mercado e vender na BitcoinTrade: ', buyMercadoBTCSellBitcoinTrade); 
	console.log('GAP vender no Mercado e comprar na BitcoinTrade: ', buyBitconTradeSellMercadoBTC);
	
	const messageBuyMercadoBTCSellBitcoinTrade = `COMPRAR na Mercado e VENDER na Bitcoin Trade.
		Valor de compra: ${mercadoBTCPrices.buy[0].price}
		Valor de venda: ${bitcoinTradePrices.sell[0].price}`;

	const messageBuyBitcoinTradeSellMercadoBTC = `COMPRAR na Bitcoin Trade e VENDER na Mercado BTC.
		Valor de compra: ${bitcoinTradePrices.buy[0].price}
		Valor de venda: ${mercadoBTCPrices.sell[0].price}`;
	
	chooseBuy(buyMercadoBTCSellBitcoinTrade, messageBuyMercadoBTCSellBitcoinTrade)
	chooseBuy(buyBitconTradeSellMercadoBTC, messageBuyBitcoinTradeSellMercadoBTC)

}

const chooseBuy = (gap, message) => {
	const gapInPercentes = gap - 1;
	if (DIFFERENCE_TO_BUY < gapInPercentes) {
		console.log('------------------------------------------------');
		console.log('------------------------------------------------');
		console.log(message);
		console.log('------------------------------------------------');
		console.log('------------------------------------------------');
	}
}

module.exports = {
    processBTCPriceBitcoinTrade,
    processPriceBTCMercadoBTC,
    gapsMercadoBTCAndBitcoinTrade
};